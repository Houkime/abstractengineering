## Nutshell
  * A (3d?) engineering roguelike

## Properties

* About basic principles of engineering:
    * objects
    * their properties
    * and how the hell to make a working machine from random parts
* Player starts in the procedural world of objects
* In the form of blocks
* Player is also a block
* The properties of objects are randomized
* THERE IS NO WARRANTY THAT THE TASK AT HAND IS POSSIBLE
    at least apriori
  * The thing though is to make a phasespace so highdimensional for unworkable start to be almost improbable.
    (that's one of the main points of the game, to illustrate almost perfect traversability of ANYTHING given enough dimensions)
* A world initially consists of 3-5 types of blocks + one (?) sentient type
* Player doesn't know anything about blocks?
* Player visually perceives abstract R G and B fields.
* Probably there is also one special invsible field of timespace warp or sth

## Abilities of blocks:

* extend (and push)
* have moving surface (to roll things/self on different directions)
* have twisting surface (to rotate)
* conduct signals
* generate signals
* be sticky (on all surfaces?)
* laser with fields R G B
* Change color (and thus near fields). Added field modification matrix? (Active medium. Enables field amplification)
* Transform into other blocks
* Duplicate
* Be sentient (all sentients can duplicate) 
  * Player block is sentient.
* self-destruct

### These abilities can be constant but also triggered by

* R G B fields
* being statically pushed
* being exposed to a signal  

## Game end variants:

* Warp field which creates a portal if space has too much WARP.
* Destroy the Sun with laser
* Pure sandbox (no end)

## New block type generation

* Can be ad infinum
* Each new block has random (?) properties. 
  * Or sth like evolving model with random mutations.
    * although last variant may be a little bit less interesting because then the endgame might be the same all the time
because player chooses to ortogonalize the blocks' properties.  
    * Although this can be mitigated by complexity of ortogonalization in multidim space)
	* ->The path to the blocks that can be game-ending may be of random length (which is quite realistic tbh)
