# Abstract Engineering

A WIP 3d engineering roguelike.  
Blockworld with block type properties regenerated anew each time.  
Because of this no designs can really be transferred between plays.  
Think about extremely abstract Infinifactory type of game but with every playthrough being with a totally different overhaul mod.  
Player can try and build a layer of abstraction to mitigate this. 
However _optimized_ designs SHOULD be very different and that's the whole point.

## Timeline

* Basic controlled-RGB block with shader-level outline [YES]
* Generator of the boilerplate world - a blockplane [YES]
* First 2 Blocktypes [YES]
* The Grid of Blocks [CURRENT]
  * Placement and type [YES]
  * Nearfield calculations [YES]
  * Ticks [YES]
  * Physics of freefalling [CURRENT]
  * Intertick movement anim
* Block properties - without transmutations and sentience
* Triggering block abilities
* BlockTypetree generation
* Transmutations
* Player block
* Sentience and Duplication
* GUI
* Endgame? (optional)
