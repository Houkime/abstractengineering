extends Spatial

var btype
var shader
var scl = 113.1
var Mat
var CurrentColor


# Called when the node enters the scene tree for the first time.
func _ready():
	Mat=ShaderMaterial.new()
	Mat.shader=shader
	$MeshInstance.set_surface_material(0,Mat)
	$MeshInstance.get_surface_material(0).set_shader_param("R",btype.R)
	$MeshInstance.get_surface_material(0).set_shader_param("G",btype.G)
	$MeshInstance.get_surface_material(0).set_shader_param("B",btype.B)
	$MeshInstance.get_surface_material(0).set_shader_param("scale",scl)
	CurrentColor=Vector3(btype.R,btype.G,btype.B)
	pass # Replace with function body.

func update_color():
	var gridnode=$"..".Grid.grid[floor(translation.x)][floor(translation.y)][floor(translation.z)]
	CurrentColor=Vector3(btype.R,btype.G,btype.B)+btype.react.xform(gridnode.field)
	if (CurrentColor.x>1.0):
		CurrentColor.x=1.0
	if (CurrentColor.y>1.0):
		CurrentColor.y=1.0
	if (CurrentColor.z>1.0):
		CurrentColor.z=1.0
	if (CurrentColor.x<0.0):
		CurrentColor.x=0.0
	if (CurrentColor.y<0.0):
		CurrentColor.y=0.0
	if (CurrentColor.z<0.0):
		CurrentColor.z=0.0
	$MeshInstance.get_surface_material(0).set_shader_param("R",CurrentColor.x)
	$MeshInstance.get_surface_material(0).set_shader_param("G",CurrentColor.y)
	$MeshInstance.get_surface_material(0).set_shader_param("B",CurrentColor.z)
	
func propagate_fields():
	var grid=$"..".Grid.grid
	var i=floor(translation.x)
	var j=floor(translation.y)
	var k=floor(translation.z)
	if (i>0):
		grid[i-1][j][k].field+=CurrentColor
	if (j>0):
		grid[i][j-1][k].field+=CurrentColor
	if (k>0):
		grid[i][j][k-1].field+=CurrentColor
	if (i<$"..".GRIDX-1):
		grid[i+1][j][k].field+=CurrentColor
	if (j<$"..".GRIDY-1):
		grid[i][j+1][k].field+=CurrentColor
	if (k<$"..".GRIDZ-1):
		grid[i][j][k+1].field+=CurrentColor
	#gridnode=$"..".Grid.grid[floor(translation.x)][floor(translation.y)][floor(translation.z)]
	