extends Object

var TypeTree
var R
var G
var B
var react
const REACTCAP=0.12

func _init():
	randomize()
	R = rand_range(0.0,1.0)
	G = rand_range(0.0,1.0)
	B = rand_range(0.0,1.0)
	var vec1 = Vector3(randf(),randf(),randf())*2 - Vector3(1.0,1.0,1.0)
	var vec2 = Vector3(randf(),randf(),randf())*2 - Vector3(1.0,1.0,1.0)
	var vec3 = Vector3(randf(),randf(),randf())*2 - Vector3(1.0,1.0,1.0)
	vec1*=REACTCAP
	vec2*=REACTCAP
	vec3*=REACTCAP
	react=Basis(vec1,vec2,vec3)
	print (String(react))
	print ("new btype = " + String(R))