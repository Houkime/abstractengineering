extends Object

var gridnode=load("res://GridNode.gd")
var grid = Array()
var gridtimer
const GRIDTICK=1.0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.

func _init(xdim,ydim,zdim):
	var counter=0
	for i in range(xdim):
		var yzarray=Array()
		for j in range(ydim):
			var zarray=Array()
			for k in range(zdim):
				zarray.append(gridnode.new())
				counter+=1
			yzarray.append(zarray)
		grid.append(yzarray)
	print (String(counter)+" blocks created")
	
	

func init_timer():
	gridtimer.wait_time=GRIDTICK
	gridtimer.connect("timeout",self,"on_timertick")
	gridtimer.start()
	#print ("started the timer?")
	#print ("paused?"+String(gridtimer.paused))

func on_timertick():
	# print("Time is ticking!!!")
	nullify_fields()
	make_fields()
	update_colors()
	
	pass

func make_fields():
	
	for a in grid:
		for b in a:
			for c in b:
				if (c.node):
					c.node.propagate_fields()

func update_colors():
	
	for a in grid:
		for b in a:
			for c in b:
				if (c.node):
					c.node.update_color()


func nullify_fields():
	for a in grid:
		for b in a:
			for c in b:
				c.field=Vector3.ZERO

func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.