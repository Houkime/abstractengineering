extends Spatial

var blockscene=load("res://Block.tscn")
var blocktype=load("res://BlockType.gd")
var blockshader=load("res://blockshader.shader")
var types= Array()
var GridClass = load("res://Grid.gd")
var Grid
const GRIDX = 10
const GRIDY = 10
const GRIDZ = 10

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.



func _ready():
	Grid=GridClass.new(GRIDX,GRIDY,GRIDZ)
	var gridtimer=Timer.new()
	Grid.gridtimer=gridtimer
	Grid.init_timer()
	add_child(gridtimer)
	randomize()
	for i in range(0,3):
		types.append( blocktype.new())
	print ("types array size =" + String(types.size()))
	for i in range(GRIDX):
		for j in range(GRIDY):
			for k in range(GRIDZ):
				var blo
				if (randf()<=0.3):
					blo=blockscene.instance()
					blo.translation=Vector3(i,j,k)
					blo.btype=types[randi() % (types.size())]
		#			print ("blo btype= "+ String(blo.btype.R))
					blo.shader=blockshader
					add_child(blo)
					Grid.grid[i][j][k].node=blo
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
