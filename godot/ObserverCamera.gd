extends Camera

var point_at=Vector3()
var phi= 0.0
var siz
const FREQ=0.1

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.

func _process(delta):	
	phi+=FREQ*2*PI*delta
	if (phi>=2*PI):
		phi=0
	translation=point_at+siz*Vector3(cos(phi),0,sin(phi))
	rotation=Vector3(0,-phi+PI/2,0)
	pass

func _ready():
	point_at=Vector3($"..".GRIDX,$"..".GRIDY,$"..".GRIDZ)
	siz=point_at.length()
	point_at=point_at/2.0
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
