shader_type spatial;
render_mode unshaded;

uniform float R;
uniform float G;
uniform float B;
uniform float scale;

void fragment()
{
	ALBEDO.rgb=vec3(R,G,B);
	if (cos(UV.x*scale/6.)>=0.9||cos(UV.y*scale/9.)>=0.9){
	ALBEDO.rgb=vec3(1,1,1);
	}
}